<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Http\Response;

Route::get('/items', function () {
    return view('recruitmentblueservicesclient::app');
});

Route::get('/items/js', function () {
    $content = file_get_contents(__DIR__ . '/../public/js/app.js');

    $response = new Response(
        $content, 200, [
            'Content-Type' => 'text/javascript; charset=utf-8',
        ]
    );

    return $response;
});

Route::get('/items/css', function () {
    $content = file_get_contents(__DIR__ . '/../public/css/style.css');

    $response = new Response(
        $content, 200, [
            'Content-Type' => 'text/css; charset=utf-8',
        ]
    );

    return $response;
});
