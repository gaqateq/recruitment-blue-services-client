<?php

namespace Gaqateq\RecruitmentBlueServicesClient\Providers;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');

        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'recruitmentblueservicesclient');
    }
}
