import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './routes';
import app from './components/app.vue'

Vue.use(VueRouter);

window.axios = require('axios/dist/axios');
window.Vue = require('vue');

new Vue({
    el: '#app',
    router,
    template: '<app/>',
    components: {app}
});
