import VueRouter from 'vue-router';
import index from './components/index.vue';
import add from './components/add.vue';
import update from './components/update.vue';

let routes = [
    { path: '/', name: 'root', component: index },
    { path: '/add', name: 'add', component: add },
    { path: '/update/:id', name: 'edit', component: update },
];

export default new VueRouter({
    routes
});
