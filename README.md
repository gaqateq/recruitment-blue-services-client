# Recruitment Blue Services Client

Recruitment Blue Services Client is a part of recruitment task for Blue Services company.

It is a frontend application that allows manage items.

[![Monthly Downloads](https://poser.pugx.org/gaqateq/recruitment-blue-services-client/d/monthly.png)](https://packagist.org/packages/gaqateq/recruitment-blue-services-client)

# Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Server](#server)
- [License](#license)

## Installation

```sh
composer require gaqateq/recruitment-blue-services-client
```

Application needs REST API with `/items` endpoint to work. You can also use ready package for this, see section [Server](#server).

## Usage

After installation package is ready to use. Application is available via URL `/items`.

## Server

As mentioned in the header this package is one part of the task. There is available a Server for this application which is available [here](https://bitbucket.org/gaqateq/recruitment-blue-services-server).

## License

Recruitment Blue Services Client is released under the MIT Licence. See the bundled LICENSE file for details.
